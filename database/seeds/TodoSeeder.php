<?php

use Illuminate\Database\Seeder;

class TodoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Todo::class, 10)->create()->each(function ($todo) {
            $schedule = new \App\Schedule;
            $schedule->todo_id = $todo->id;
            $schedule->user_id = $todo->user_id;
            $schedule->time = $todo->reminder_time;
            $schedule->save();
        });
    }
}
