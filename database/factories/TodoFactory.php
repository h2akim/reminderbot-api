<?php

use Faker\Generator as Faker;

$factory->define(App\Todo::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'todo' => $faker->realText(64),
        'reminder_time' => $faker->dateTimeInInterval('now', $interval = '+ 15 minutes', 'UTC'),
        'unique_id' => hash('sha256', uniqid('rbot', true))
    ];
});
