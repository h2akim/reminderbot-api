<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Lcobucci\JWT\Parser;
use Illuminate\Support\Facades\Route;
use Auth;

class AuthController extends Controller
{
    private $client;

    public function __construct()
    {
        $this->client = \DB::table('oauth_clients')->where('id', 2)->first();
    }

    public function login(Request $request)
    {
        $request->request->add([
            'grant_type' => 'password',
            'client_id' => $this->client->id,
            'client_secret' => $this->client->secret,
            'username' => $request->username,
            'password' => $request->password,
            'scope' => '*'
        ]);

        $proxyRequest = Request::create(
            'oauth/token',
            'POST'
        );

        return Route::dispatch($proxyRequest);
    }

    public function refreshToken(Request $request)
    {
        $request->request->add([
            'grant_type' => 'refresh_token',
            'refresh_token' => $request->refresh_token,
            'client_id' => $this->client->id,
            'client_secret' => $this->client->secret,
        ]);
        $proxyRequest = Request::create(
            'oauth/token',
            'POST'
        );
        return Route::dispatch($proxyRequest);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        $request->user()->token()->delete();
        return response()->json([ 'message' => 'Succesfully logout' ]);
    }

    public function register(Request $request)
    {
        $data = [
            'name' => $request->name,
            'password' => $request->password,
            'email' => $request->email,
            'password_confirmation' => $request->confirmed
        ];
        $validator = \Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $user = new \App\User;
        $data['password'] = bcrypt($data['password']);
        try {
            \DB::transaction(function () use ($data, $user) {
                $user = $user->create($data);
            });
        } catch (Throwable $e) {
            return response()->json([], 500);
        }

        return response()->json([ 'message' => 'User created' ]);
    }
}
