<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Todo;

class TodoController extends Controller
{
    public function getTodoList(Request $request)
    {
        $date = $request->date ? $request->date : null;

        $todos = Todo::where('user_id', $request->user()->id);

        if ($date) {
            $todos = $todos->whereDate('reminder_time', $date);
        }

        $todos = $todos->orderBy('reminder_time', 'asc')->get();

        return response()->json($todos);
    }

    // Handle task addition
    public function addTodo(Request $request)
    {
        $todo = new Todo;
        try {
            \DB::transaction(function () use ($request, $todo) {
                $todo->todo = $request->todo;
                $todo->user_id = \Auth::id();
                $todo->reminder_time = $request->reminder_time;
                $todo->unique_id = hash('sha256', uniqid('rbot', true));
                $todo->save();
            });
        } catch (Throwable $e) {
            return response()->json([], 500);
        }
        return response()->json($todo);
    }

    public function toggleTelegram(Request $request)
    {
    }

    public function toggleComplete(Request $request)
    {
        $todo = Todo::where('user_id', $request->user()->id);
        if ($request->uniqueId) {
            $key = 'unique_id';
        } else {
            $key = 'id';
        }
        $value = $request->{$key};
        $todo = $todo->where($key, $value)->first();

        if (!$todo) {
            return response()->json([], 404);
        }

        try {
            $todo->completed = !$todo->completed;
            $todo->save();
        } catch (Throwable $e) {
            return response()->json([], 500);
        }

        return response([ 'status' => 'Success', 'data' => $todo ]);
    }

    public function deleteTodo(Request $request)
    {
        $todo = Todo::where('user_id', $request->user()->id);
        if ($request->uniqueId) {
            $key = 'uniqueId';
        } else {
            $key = 'id';
        }
        $value = $request->{$key};
        $todo = $todo->where($key, $value)->first();

        // Delete
        try {
            \DB::transaction(function () use ($todo) {
                $todo->delete();
            });
        } catch (Throwable $e) {
            return response()->json([], 500);
        }

        return response([ 'status' => 'Success ']);
    }
}
