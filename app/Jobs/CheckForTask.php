<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Bus\Dispatcher;

use App\Todo;
use App\Schedule;
use Carbon\Carbon;
use App\Jobs\SendReminder;

class CheckForTask implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    // * * * * * php /Users/Hakim/Documents/Programming/projects/remainderbot-api/artisan schedule:run >> /dev/null 2>&1

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Handle task checking
        $ticker = 0;
        while ($ticker < 6) {
            // Check for task that need to be send reminder in next 10 minutes then create a job out of it
            $tasks = [];

            $nonScheduledReminders = Schedule::where('actual_time', null)->whereBetween('time', [ Carbon::now(), Carbon::now()->addMinutes(10) ])->orderBy('time', 'asc')->get();
            // group it by user_id first
            foreach ($nonScheduledReminders as $reminder) {
                $tasks[$reminder->user_id][] = $reminder;
            }

            // process all the task put it in job
            foreach ($tasks as $task) {
                $last_time = null;
                $last_second = 0;
                foreach ($task as $subtask) {
                    $diff = $subtask->time->timestamp - Carbon::now()->timestamp;

                    if (!$last_time) {
                        if ($diff > 180) {
                            $seconds = rand(0, 180);
                            $subtask->seconds = $seconds;
                            echo "--First--\n";
                            echo "Diff: ".$diff."\n";
                            echo "Seconds: ".$seconds."\n";
                            echo "----\n";
                            $subtask->actual_time = Carbon::now()->addSeconds($seconds);
                            $subtask->save();
                            $last_time = $subtask->actual_time->timestamp;
                            $last_second = $seconds;
                            $id = app(Dispatcher::class)->dispatch((new SendReminder($subtask))->delay($seconds));
                            $subtask->job_id = $id;
                            $subtask->save();
                        }
                    } else {
                        // last time (earlier) -> current time
                        $diff = $subtask->time->timestamp - $last_time;
                        $seconds = $last_second + rand(0, $diff);
                        echo "--After--\n";
                        echo "Diff: ".$diff."\n";
                        echo "Seconds: ".$seconds."\n";
                        echo "----\n";
                        $subtask->seconds = $seconds;
                        $subtask->actual_time = Carbon::now()->addSeconds($seconds);
                        $subtask->save();
                        $last_time = $subtask->actual_time->timestamp;
                        $last_second = $seconds;
                        $id = app(Dispatcher::class)->dispatch((new SendReminder($subtask))->delay($seconds));
                        $subtask->job_id = $id;
                        $subtask->save();
                    }
                }
            }
            $ticker++;
            // QUICK HACK - rerun all the checking after 10 seconds sleep as cron only support minutes atleast
            echo 'tidur';
            sleep(10);
        }
    }
}
