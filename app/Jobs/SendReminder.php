<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendReminder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $schedule;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(\App\Schedule $task)
    {
        $this->schedule = $task;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->schedule->sent = true;
        $this->schedule->save();
        // $bot_token = $todo->user->userbot->bot->token;
        // $bot_chat_id = $todo->user->userbot->chat_id;

        // try {
        //     $response = $http->post(env('TELEGRAM_API').$bot_token.'/sendMessage', [
        //         'json' => [
        //             'chat_id' => $bot_chat_id,
        //             'text' => $this->schedule->todo->todo,
        //         ],
        //     ]);
        //     $responseBody = $response->getBody();
        // } catch (\GuzzleHttp\Exception\ClientException $e) {
        //     $responseBody = $e->getResponse()->getBody()->getContents();
        // }
    }
}
