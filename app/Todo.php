<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    protected $hidden = [ 'user_id', 'sent_to_telegram', 'sent', 'created_at', 'updated_at' ];

    protected $dates = [ 'created_at', 'updated_at', 'reminder_time' ];

    public function user()
    {
        return $this->belongsTo(App\User::class);
    }
}
