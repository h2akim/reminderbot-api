<?php

namespace App\Console\Commands;

use App\User;
use App\Jobs\CheckForTask;
use Illuminate\Console\Command;

class TestScheduling extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:scheduling';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Schedule for telegram';

    /**
     * Create a new command instance.
     *
     * @param  DripEmailer  $drip
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dispatch(new CheckForTask);
    }
}
