<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $dates = [ 'created_at', 'updated_at', 'actual_time', 'time' ];

    public function todo()
    {
        return $this->belongsTo(App\Todo::class);
    }

    public function user()
    {
        return $this->belongsTo(App\User::class);
    }
}
